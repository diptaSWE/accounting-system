<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Acc Fixed Asset</title>
<script type="text/javascript">

$(function() {
		$("#purchase_date").datepicker({
			dateFormat : "yy-mm-dd"

		});
});

$(document).ready(function(){
	$("#submit").click(function(event) {
		if(validator()){
			var data={};
			
			data["itemId"]         	= $("#itemId").val();
			data["purchaseDt"]		= $("#purchase_date").val();
			data["purchaseQty"]    = $("#purchaseQty").val();
			data["purchaseRate"]	= $("#purchaseRate").val();
			data["depriPercent"]	= $("#depriPercent").val();
			data["salvageValue"]	= $("#salvageValue").val();
			data["expireStatus"]	= $("#expireStatus").val();
			alert(JSON.stringify(data));
			$.ajax({
	             type: "POST",
	             url: "saveFixedAsset",
	             data: JSON.stringify(data),
	             contentType: "application/json; charset=utf-8",             
	             success: function (successData) {
	            				                 
	                $("#successMessage").html(successData); 
	                $("#successMessage").removeClass("hidden");
	                $("#errorMessage").addClass("hidden");
	                      
	             },
	             error: function (error) {
	            	 /* $("#errorMessage").html("Error: Record Not Saved");
	            	 $("#errorMessage").removeClass("hidden");
	                 $("#submit").prop("disabled", false); */
	                 alert(JSON.stringify(error));
	             }
			});
		}
	});
});

function validator(){
	var itemId     		= $.trim($("#itemId").val());
	var purchaseDate	= $.trim($("#purchase_date").val());
	var purchaseQty		= $.trim($("#purchaseQty").val());
	var purchaseRate	= $.trim($("#purchaseRate").val());
	var depriPercent	= $.trim($("#depriPercent").val());
	var salvageValue	= $.trim($("#salvageValue").val());
	var expireStatus	= $.trim($("#expireStatus").val());
	
	if(itemId == ""){		
   	 	$("#errorMessage").html("Please Enter a item id.");
   	    $("#errorMessage").removeClass("hidden");
   	 	
   	    $("#successMessage").addClass("hidden");	         	      	 
        return false;		                
	}
	else if(purchaseDate == ""){		
   	 	$("#errorMessage").html("Please Enter purchase date.");
   	    $("#errorMessage").removeClass("hidden");
   	 	
   	    
   	    $("#successMessage").addClass("hidden");	         	      	 
        return false;		                
	}
	else if(purchaseQty == ""){		
   	 	$("#errorMessage").html("Please Enter purchase quantity.");
   	    $("#errorMessage").removeClass("hidden");
   	 	
   	    
   	    $("#successMessage").addClass("hidden");	         	      	 
        return false;		                
	}
	else if(purchaseRate == ""){		
   	 	$("#errorMessage").html("Please Enter purchase rate.");
   	    $("#errorMessage").removeClass("hidden");
   	 	
   	    
   	    $("#successMessage").addClass("hidden");	         	      	 
        return false;		                
	}
	else if(depriPercent == ""){		
   	 	$("#errorMessage").html("Please Enter Depreciation Percent.");
   	    $("#errorMessage").removeClass("hidden");
   	 	
   	    
   	    $("#successMessage").addClass("hidden");	         	      	 
        return false;		                
	}
	else if(salvageValue == ""){		
   	 	$("#errorMessage").html("Please Enter Salvage value.");
   	    $("#errorMessage").removeClass("hidden");
   	    $("#successMessage").addClass("hidden");	         	      	 
        return false;		                
	}
	else if(expireStatus == "null"){		
   	 	$("#errorMessage").html("Please Select Status.");
   	    $("#errorMessage").removeClass("hidden");
   	    $("#successMessage").addClass("hidden");	         	      	 
        return false;		                
	}
	return true;
	
}
</script>
</head>
<body>

<div class="col-md-10 col-lg-10 col-sm-10 col-md-offset-1" style="padding-top:80px;">
	<div class="panel panel-primary">
      <div class="panel-heading"><b>Fixed Asset</b></div>
      <div class="panel-body">
      
      		<div class="col-md-12">
      			<div id="successMessage" align="center" class="hidden alert alert-success"></div>
				<div id="errorMessage" align="center" class="hidden alert alert-danger"></div>
      		</div>
      
      		<div class="col-md-12" style="padding-top:5px">
      		<div class="col-md-2"><b>Item ID</b></div>
      		<div class="col-md-4"><input type="text" id="itemId" class="form-control"></div>
      		<div class="col-md-2"><b>Purchase Date</b></div>
      		<div class="col-md-4"><input type="text" id="purchase_date" class="form-control"></div>
      		</div>
      		<div class="col-md-12" style="padding-top:5px">
      		<div class="col-md-2"><b>purchase Quantity</b></div>
      		<div class="col-md-4"><input type="text" id="purchaseQty" class="form-control"></div>
      		<div class="col-md-2"><b>purchase Rate</b></div>
      		<div class="col-md-4"><input type="text" id="purchaseRate" class="form-control"></div>
      		</div>
      		<div class="col-md-12" style="padding-top:5px">
      		<div class="col-md-2"><b>Depreciation Percent</b></div>
      		<div class="col-md-4"><input type="text" id="depriPercent" class="form-control"></div>
      		<div class="col-md-2"><b>Salvage Value</b></div>
      		<div class="col-md-4"><input type="text" id="salvageValue" class="form-control"></div>
      		</div>
      		
      		<div class="col-md-12" style="padding-top:5px">
      		<div class="col-md-2"><b>Expire Status</b></div>
      		<div class="col-md-10">
      			<select class="form-control" id="expireStatus">
      			<option value="null">Select</option>
      			<option value="1">Active</option>
      			<option value="2">DeActive</option>
      			</select>
      		</div>
      		</div>
      		
      		<div class="col-md-5"></div>
      		<div class="col-md-6">
				<input id="submit" type="button" name="submit" value="Save" class="btn btn-primary" />
				<input id="btnClear" type="button" name="btnClear" value="Clear" class="btn btn-warning" />
			</div>
      </div>
    </div>
</div>
</body>
</html>