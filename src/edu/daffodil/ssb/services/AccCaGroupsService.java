package edu.daffodil.ssb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.daffodil.ssb.dao.CaGroups;
import edu.daffodil.ssb.dao.CaGroupsDao;



@Service("accCaGroupsService")
public class AccCaGroupsService {
	
	private CaGroupsDao accCaGroupsDao;
	
	@Autowired
	public void setAccCaGroupsDao(CaGroupsDao accCaGroupsDao) {
		this.accCaGroupsDao = accCaGroupsDao;
	}
	public List<CaGroups> showAccCaGroup() {
		
		return accCaGroupsDao.showAccCaGroups();
	}
	public void saveAccCaGroups(CaGroups accCaGroups) {
		accCaGroupsDao.saveAccCaGroup(accCaGroups);
		
	}
	public List<CaGroups> showAccCaGroupTable() {
		
		return accCaGroupsDao.showAccCaGroupsTable();
	}
	public void updateAccCaGroups(CaGroups accCaGroups) {
		accCaGroupsDao.updateAccCaGroups(accCaGroups);
		
	}
	public CaGroups showAccCaGroupById(int cagId) {
		// TODO Auto-generated method stub
		return accCaGroupsDao.showAccCaGroupsById(cagId);
	}
	public void deleteAccCaGroups(CaGroups cagId) {
		// TODO Auto-generated method stub
		accCaGroupsDao.deleteAccCaGroups(cagId);
	}
	
	


}
