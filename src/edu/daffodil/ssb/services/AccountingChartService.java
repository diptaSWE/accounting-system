package edu.daffodil.ssb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.daffodil.ssb.dao.ChartOfAccount;
import edu.daffodil.ssb.dao.ChartOfAccountDao;

@Service("AccCaService")
public class AccountingChartService {
	private ChartOfAccountDao accountingChartDao;
	
	
	@Autowired
	public void setAccountingChartDao(ChartOfAccountDao accountingChartDao) {
		this.accountingChartDao = accountingChartDao;
	}



	public ChartOfAccount getControlHeadMax(String casgId) {
		return accountingChartDao.getControlHeadMax(casgId);
	}
	
	public ChartOfAccount getSubControlHeadMax(String casgId, String caId){
		return accountingChartDao.getSubControlHeadMax(casgId, caId);
	}
	
	public ChartOfAccount getSubSubControlHeadMax(String casgId, String caId){
		return accountingChartDao.getSubSubControlHeadMax(casgId, caId);
	}



	public void saveorupdate(ChartOfAccount accountingChartTemp) {
		accountingChartDao.saveorupdate(accountingChartTemp);
		
	}



	public List<ChartOfAccount> showControlhead(String casgId) {
		return accountingChartDao.showControlhead(casgId);
	}



	public List<ChartOfAccount> showSubControlhead(String caId) {
		return accountingChartDao.showSubControlhead(caId);
	}



	public List<ChartOfAccount> showSubSubControlhead(String caId) {
		return accountingChartDao.showSubSubControlhead(caId);
	}
	

}
