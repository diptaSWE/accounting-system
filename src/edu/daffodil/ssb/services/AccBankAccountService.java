package edu.daffodil.ssb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.daffodil.ssb.dao.BankAccountDao;
import edu.daffodil.ssb.dao.BankInfo;
import edu.daffodil.ssb.dao.BankAccount;
import edu.daffodil.ssb.dao.FinYear;



@Service("accBankAccountService")
public class AccBankAccountService {
	
	
	private BankAccountDao accBankAccountDao;
	
	@Autowired
	public void setAccBankAccountDao(BankAccountDao accBankAccountDao) {
		this.accBankAccountDao =   accBankAccountDao;
	}
	

	public void saveOrUpdate(BankAccount accBankAccount) {
		// TODO Auto-generated method stub
		accBankAccountDao.saveOrUpdate(accBankAccount);
		
	}


	public List<FinYear> showCompanyId() {
		// TODO Auto-generated method stub
		return accBankAccountDao.showCompanyId();
	}


	public List<BankInfo> showAccBankId() {
		
		return accBankAccountDao.showAccBankId();
	}
	
	public List<BankInfo> getBankList(){
		return accBankAccountDao.getBankList();
		
	}


	public List<BankAccount> showAccBankAccountInfo() {
		// TODO Auto-generated method stub
		return accBankAccountDao.showAccBankAccountInfo();
	}


	public BankAccount deleteAccBankAccountBy(int id) {
		// TODO Auto-generated method stub
		return accBankAccountDao.deleteAccBankAccountBy(id);
	}


	public void deleteAccBankAccount(BankAccount id) {
		// TODO Auto-generated method stub
		accBankAccountDao.deleteAccBankAccount(id);
		
	}


	


	
	
	

}
