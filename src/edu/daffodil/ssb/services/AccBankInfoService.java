package edu.daffodil.ssb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.daffodil.ssb.dao.BankInfo;
import edu.daffodil.ssb.dao.BankInfoDao;



@Service("accBankInfoService")

public class AccBankInfoService {
	
	
	private BankInfoDao accBankInfoDao;
	
	
	@Autowired
	public void setAccBankinfoDao(BankInfoDao accBankInfoDao) {
		this. accBankInfoDao =  accBankInfoDao;
	}

	

	public void saveOrUpdate(BankInfo accBankInfo) {
		// TODO Auto-generated method stub
		accBankInfoDao.saveOrUpdate(accBankInfo);
		
	}



	public List<BankInfo> showAccBankInfo() {
		
		return accBankInfoDao.showAccBankInfo();
	}


     public BankInfo deleteAccBankInfoBy(int id) {
		
		return accBankInfoDao.deleteAccBankInfoBy(id) ;
	}



	public void deleteAccBankInfo(BankInfo id) {
		 accBankInfoDao.deleteAccBankInfo(id) ;
		
	}

}
