package edu.daffodil.ssb.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.daffodil.ssb.dao.FixedAsset;
import edu.daffodil.ssb.dao.FixedAssetDao;

@Service("fixedAssetService")
public class FixedAssetService {
	@Autowired
	private FixedAssetDao fixedAssetDao;;
	
	public void setFixedAssetDao(FixedAssetDao fixedAssetDao) {
		this.fixedAssetDao = fixedAssetDao;
	}

	public void saveOrUpdate(FixedAsset fixedAsset) {
		// TODO Auto-generated method stub
		
		fixedAssetDao.saveOrUpdate(fixedAsset);
	}

}
