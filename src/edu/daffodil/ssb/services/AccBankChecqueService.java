package edu.daffodil.ssb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import edu.daffodil.ssb.dao.BankChecque;
import edu.daffodil.ssb.dao.BankChecqueDao;
import edu.daffodil.ssb.dao.BankInfo;
import edu.daffodil.ssb.dao.BankAccount;


@Service("accBankChecqueService")
public class AccBankChecqueService {
	
private BankChecqueDao accBankChecqueDao;
	
	@Autowired
	public void setAccBankChecqueDao(BankChecqueDao  accBankChecqueDao) {
		this.accBankChecqueDao =   accBankChecqueDao;
	}

	public java.util.List<BankInfo> showBankName() {
		// TODO Auto-generated method stub
		return accBankChecqueDao.showBankName();
	}

	

	public List<BankAccount> showAccBankAccount(int id) {
		// TODO Auto-generated method stub
		return accBankChecqueDao.showAccBankAccount(id);
	}

	

	public void saveOrUpdate(BankChecque accBankchecuqe) {
		// TODO Auto-generated method stub
		accBankChecqueDao.SaveOrUpdate(accBankchecuqe);
		
	}

	public List<BankChecque> showAccBankCheque() {
		// TODO Auto-generated method stub
		return  accBankChecqueDao.showAccBankCheque();
	}

	
	
}
