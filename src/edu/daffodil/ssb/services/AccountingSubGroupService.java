package edu.daffodil.ssb.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.daffodil.ssb.dao.CaSubGroups;
import edu.daffodil.ssb.dao.CaSubGroupsDao;

@Service("accCaSubGroupsService")
public class AccountingSubGroupService {
	
	private CaSubGroupsDao accCaSubGroupsDao;

	@Autowired
	public void setAccCaSubGroupsDao(CaSubGroupsDao accCaSubGroupsDao) {
		this.accCaSubGroupsDao = accCaSubGroupsDao;
	}

	public List<CaSubGroups> showAccCaSubGroupsByGroups(int cagId) {
		return accCaSubGroupsDao.showAccCaSubGroupsByGroups(cagId);
	}

}
