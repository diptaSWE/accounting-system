package edu.daffodil.ssb.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
@Component("chartOfAccountDao")
public class ChartOfAccountDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session(){
		return sessionFactory.getCurrentSession();
	}

	public ChartOfAccount getControlHeadMax(String casgId) {
		
		ChartOfAccount maxObject = null;
 		DetachedCriteria criteria = DetachedCriteria.forClass(ChartOfAccount.class);
		criteria.add(Restrictions.isNull("caParent"));
		criteria.add(Restrictions.eq("casgId", casgId));
		criteria.addOrder(Order.desc("caId"));
		
		try{
		
		maxObject = (ChartOfAccount) criteria.getExecutableCriteria(session()).setMaxResults(1).uniqueResult();
		return maxObject;
		}
		catch(NullPointerException e){
			System.out.println("Try catch Exception");
			return null;
		}
		
	}

	public ChartOfAccount getSubControlHeadMax(String casgId, String caId) {
		ChartOfAccount maxObject;
		DetachedCriteria criteria = DetachedCriteria.forClass(ChartOfAccount.class);
		criteria.add(Restrictions.isNotNull("caParent"));
		criteria.add(Restrictions.eq("caLevel", Short.parseShort("2")));
		criteria.add(Restrictions.eq("casgId", casgId));
		criteria.add(Restrictions.ilike("caId", caId +"%"));
		criteria.addOrder(Order.desc("caId"));
		
		try{
			maxObject = (ChartOfAccount) criteria.getExecutableCriteria(session()).setMaxResults(1).uniqueResult();
			System.out.println("---"+maxObject);
			return maxObject;
		}
		catch(NullPointerException e){
			System.out.println("Try catch Exception");
			return null;
		}
	}
	
	public ChartOfAccount getSubSubControlHeadMax(String casgId, String caId) {
		ChartOfAccount maxObject;
		DetachedCriteria criteria = DetachedCriteria.forClass(ChartOfAccount.class);
		criteria.add(Restrictions.isNotNull("caParent"));
		criteria.add(Restrictions.eq("caLevel", Short.parseShort("3")));
		criteria.add(Restrictions.ilike("caId", caId +"%"));
		criteria.addOrder(Order.desc("caId"));
		
		try{
			maxObject = (ChartOfAccount) criteria.getExecutableCriteria(session()).setMaxResults(1).uniqueResult();
			System.out.println("---"+maxObject);
			return maxObject;
		}
		catch(NullPointerException e){
			System.out.println("Try catch Exception");
			return null;
		}
	}

	public void saveorupdate(ChartOfAccount accountingChartTemp) {
		// TODO Auto-generated method stub
		session().saveOrUpdate(accountingChartTemp);
	}

	@SuppressWarnings("unchecked")
	public List<ChartOfAccount> showControlhead(String casgId) {
		// TODO Auto-generated method stub
		DetachedCriteria criteria = DetachedCriteria.forClass(ChartOfAccount.class);
		criteria.add(Restrictions.eq("casgId", casgId));
		criteria.add(Restrictions.eq("caLevel", Short.parseShort("1")));
		return criteria.getExecutableCriteria(session()).list();
		
	}

	@SuppressWarnings("unchecked")
	public List<ChartOfAccount> showSubControlhead(String caId) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ChartOfAccount.class);
		criteria.add(Restrictions.eq("caLevel", Short.parseShort("2")));
		criteria.add(Restrictions.ilike("caId", caId +"%"));
		return criteria.getExecutableCriteria(session()).list();
	}

	@SuppressWarnings("unchecked")
	public List<ChartOfAccount> showSubSubControlhead(String caId) {
		DetachedCriteria criteria = DetachedCriteria.forClass(ChartOfAccount.class);
		criteria.add(Restrictions.eq("caLevel", Short.parseShort("3")));
		criteria.add(Restrictions.ilike("caId", caId +"%"));
		return criteria.getExecutableCriteria(session()).list();
	}
	

}
