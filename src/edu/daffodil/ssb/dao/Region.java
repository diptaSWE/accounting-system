package edu.daffodil.ssb.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="regions")
public class Region {
	
	@Id
	@Column(name="region_id")
	private int regionId;
	
	@Column(name="region_type")
	private short regionType;
	
	@Column(name="region_name")
	private String regionName;
	
	@Column(name="region_country")
	public int regionCountry;
	
	@Column(name="region_parent")
	public int regionParent;

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public short getRegionType() {
		return regionType;
	}

	public void setRegionType(short regionType) {
		this.regionType = regionType;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public int getRegionCountry() {
		return regionCountry;
	}

	public void setRegionCountry(int regionCountry) {
		this.regionCountry = regionCountry;
	}

	public int getRegionParent() {
		return regionParent;
	}

	public void setRegionParent(int regionParent) {
		this.regionParent = regionParent;
	}

	@Override
	public String toString() {
		return "Region [regionId=" + regionId + ", regionType=" + regionType + ", regionName=" + regionName
				+ ", regionCountry=" + regionCountry + ", regionParent=" + regionParent + "]";
	}
	
	

}
