package edu.daffodil.ssb.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="acc_fixed_asset_master")
public class FixedAssetMaster {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="item_id")
	private String itemId;
	
	@Column(name="purchase_dt")
	private String date;
	
	@Column(name="purchase_qty")
	private int purchaseQty;
	
	@Column(name="purchase_rate")
	private double purchaseRate;
	
	@Column(name="depri_percent")
	private double depriPercent;
	
	@Column(name="salvage_value")
	private double salvageValue;
	
	@Column(name="expire_status")
	private short expireStatus;
	
	@Column(name="created_by")
	private String createdBy;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getPurchaseQty() {
		return purchaseQty;
	}

	public void setPurchaseQty(int purchaseQty) {
		this.purchaseQty = purchaseQty;
	}

	public double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public double getDepriPercent() {
		return depriPercent;
	}

	public void setDepriPercent(double depriPercent) {
		this.depriPercent = depriPercent;
	}

	public double getSalvageValue() {
		return salvageValue;
	}

	public void setSalvageValue(double salvageValue) {
		this.salvageValue = salvageValue;
	}

	public short getExpireStatus() {
		return expireStatus;
	}

	public void setExpireStatus(short expireStatus) {
		this.expireStatus = expireStatus;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Override
	public String toString() {
		return "FixedAssetMaster [id=" + id + ", itemId=" + itemId + ", date=" + date + ", purchaseQty=" + purchaseQty
				+ ", purchaseRate=" + purchaseRate + ", depriPercent=" + depriPercent + ", salvageValue=" + salvageValue
				+ ", expireStatus=" + expireStatus + ", createdBy=" + createdBy + "]";
	}
	
	
	
	
	

}
