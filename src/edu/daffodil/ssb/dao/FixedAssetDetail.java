package edu.daffodil.ssb.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.SqlFragmentAlias;

@Entity
@Table(name="acc_fixed_asset_detail")
public class FixedAssetDetail {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="master_id")
	private int masterId;
	
	@Column(name="depri_mmyy")
	private String depriMmYy;
	
	@Column(name="depri_amount")
	private String depriAmmount;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getMasterId() {
		return masterId;
	}

	public void setMasterId(int masterId) {
		this.masterId = masterId;
	}

	public String getDepriMmYy() {
		return depriMmYy;
	}

	public void setDepriMmYy(String depriMmYy) {
		this.depriMmYy = depriMmYy;
	}

	public String getDepriAmmount() {
		return depriAmmount;
	}

	public void setDepriAmmount(String depriAmmount) {
		this.depriAmmount = depriAmmount;
	}

	@Override
	public String toString() {
		return "FixedAssetDetail [id=" + id + ", masterId=" + masterId + ", depriMmYy=" + depriMmYy + ", depriAmmount="
				+ depriAmmount + "]";
	}
	
	
}
