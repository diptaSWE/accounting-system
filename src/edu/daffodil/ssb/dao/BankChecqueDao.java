package edu.daffodil.ssb.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
@Component("accBankChecqueDao")
public class BankChecqueDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session(){
		return sessionFactory.getCurrentSession();
	}
	
	@SuppressWarnings("unchecked")
	public List<BankInfo> showBankName() {
		  DetachedCriteria criteria = DetachedCriteria.forClass(BankInfo.class); 
			
			return criteria.getExecutableCriteria(session()).list();
	}

	public BankAccount showBankAccount(int bankValue) {
		DetachedCriteria criteria = DetachedCriteria.forClass(BankAccount.class); 
		criteria.add(Restrictions.eq("id", bankValue));
		return (BankAccount) criteria.getExecutableCriteria(session()).uniqueResult();
	}
	
	@SuppressWarnings({ "unchecked" })
	public List<BankAccount> showAccBankAccount(int id) {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(BankAccount.class);
		criteria.add(Restrictions.eq("bankid", id));
		return  criteria.getExecutableCriteria(session()).list();
	}

	public void SaveOrUpdate(BankChecque accBankchecuqe) {
		// TODO Auto-generated method stub
		
		session().saveOrUpdate(accBankchecuqe);
	}

	@SuppressWarnings("unchecked")
	public List<BankChecque> showAccBankCheque() {
        DetachedCriteria criteria = DetachedCriteria.forClass(BankChecque.class); 
		
		return criteria.getExecutableCriteria(session()).list();
	}

}
