package edu.daffodil.ssb.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Repository
@Transactional
@Component("AccCaGroupsDao")
public class CaGroupsDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public Session session(){
		return sessionFactory.getCurrentSession();
	}
	@RequestMapping(value="/aveAccCaGroups",method=RequestMethod.POST)
	public void saveAccCaGroup(CaGroups acccagroups){
		session().save(acccagroups);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaGroups> showAccCaGroups() {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(CaGroups.class); 
		criteria.add(Restrictions.isNull("cagParent"));
		return criteria.getExecutableCriteria(session()).list();
		
	}
	@SuppressWarnings("unchecked")
	public List<CaGroups> showAccCaGroupsTable() {
		DetachedCriteria criteria = DetachedCriteria.forClass(CaGroups.class); 
		criteria.add(Restrictions.isNotNull("cagParent"));
		return criteria.getExecutableCriteria(session()).list();
		
	}
	public void updateAccCaGroups(CaGroups accCaGroups) {
		session().update(accCaGroups);
		
	}
	public CaGroups showAccCaGroupsById(int cagId) {
		
		DetachedCriteria criteria = DetachedCriteria.forClass(CaGroups.class);
		criteria.add(Restrictions.eq("cagId", cagId));
		return (CaGroups) criteria.getExecutableCriteria(session()).uniqueResult();
	}
	public void deleteAccCaGroups(CaGroups cagId) {
		session().delete(cagId);
	}
}
