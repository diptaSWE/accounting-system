package edu.daffodil.ssb.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="acc_report_head_detail")
public class ReportHeadDeatail {
	
	@Id
	@Column(name="head_id")
	private int headId;
	
	@Column(name="acc_head")
	private String accHead;

	public int getHeadId() {
		return headId;
	}

	public void setHeadId(int headId) {
		this.headId = headId;
	}

	public String getAccHead() {
		return accHead;
	}

	public void setAccHead(String accHead) {
		this.accHead = accHead;
	}

	@Override
	public String toString() {
		return "ReportHeadDeatail [headId=" + headId + ", accHead=" + accHead + "]";
	}
	
	

}
