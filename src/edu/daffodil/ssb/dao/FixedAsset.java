package edu.daffodil.ssb.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="acc_fixed_asset_master")
public class FixedAsset {
	
	@Id
	@Column(name ="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="item_id")
	private String itemId;
	
	@Column(name="purchase_dt", nullable=true)
	private String purchaseDt;
	
	@Column(name="purchase_qty")
	private double purchaseQty;
	
	@Column(name="purchase_rate")
	private double purchaseRate;
	
	@Column(name="depri_percent")
	private double depriPercent;
	
	@Column(name="salvage_value")
	private double salvageValue;
	
	@Column(name="expire_status")
	private short expireStatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getPurchaseDt() {
		return purchaseDt;
	}

	public void setPurchaseDt(String purchaseDt) {
		this.purchaseDt = purchaseDt;
	}

	public double getPurchaseQty() {
		return purchaseQty;
	}

	public void setPurchaseQty(double purchaseQty) {
		this.purchaseQty = purchaseQty;
	}

	public double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public double getDepriPercent() {
		return depriPercent;
	}

	public void setDepriPercent(double depriPercent) {
		this.depriPercent = depriPercent;
	}

	public double getSalvageValue() {
		return salvageValue;
	}

	public void setSalvageValue(double salvageValue) {
		this.salvageValue = salvageValue;
	}

	public short getExpireStatus() {
		return expireStatus;
	}

	public void setExpireStatus(short expireStatus) {
		this.expireStatus = expireStatus;
	}

	@Override
	public String toString() {
		return "FixedAsset [id=" + id + ", itemId=" + itemId + ", purchaseDt=" + purchaseDt + ", purchaseQty="
				+ purchaseQty + ", purchaseRate=" + purchaseRate + ", depriPercent=" + depriPercent + ", salvageValue="
				+ salvageValue + ", expireStatus=" + expireStatus + "]";
	}
	

	
	
	
}
