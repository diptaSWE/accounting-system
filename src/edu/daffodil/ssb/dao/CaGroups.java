package edu.daffodil.ssb.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="acc_ca_groups")
public class CaGroups {

@Id
@Column(name="cag_id")
@GeneratedValue(strategy = GenerationType.IDENTITY)
private int cagId;

@Column(name="cag_name")
private String cagName;

@Column(name="cag_parent")
private String cagParent;

public int getCagId() {
	return cagId;
}

public void setCagId(int cagId) {
	this.cagId = cagId;
}

public String getCagName() {
	return cagName;
}

public void setCagName(String cagName) {
	this.cagName = cagName;
}

public String getCagParent() {
	return cagParent;
}

public void setCagParent(String cagParent) {
	this.cagParent = cagParent;
}

@Override
public String toString() {
	return "CaGroups [cagId=" + cagId + ", cagName=" + cagName + ", cagParent=" + cagParent + "]";
}



}
