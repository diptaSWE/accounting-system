package edu.daffodil.ssb.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="acc_ca_sub_groups")
public class CaSubGroups {
	
	@Id
	@Column(name="casg_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int casgId;
	
	@Column(name="casg_name")
	private String casgName;
	
	@Column(name="cag_id")
	private int cagId;

	public int getCasgId() {
		return casgId;
	}

	public void setCasgId(int casgId) {
		this.casgId = casgId;
	}

	public String getCasgName() {
		return casgName;
	}

	public void setCasgName(String casgName) {
		this.casgName = casgName;
	}

	public int getCagId() {
		return cagId;
	}

	public void setCagId(int cagId) {
		this.cagId = cagId;
	}

	@Override
	public String toString() {
		return "CaSubGroups [casgId=" + casgId + ", casgName=" + casgName + ", cagId=" + cagId + "]";
	}

	
	
	
}
