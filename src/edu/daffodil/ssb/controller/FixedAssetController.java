package edu.daffodil.ssb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.daffodil.ssb.dao.FixedAsset;
import edu.daffodil.ssb.services.FixedAssetService;

@Controller
public class FixedAssetController {

	@Autowired
	private FixedAssetService fixedAssetService;
	
	@RequestMapping("/fixedasset")
	public String showFixedasset(){
		return "fixedasset";
	}
	
	@RequestMapping(value="/saveFixedAsset",method=RequestMethod.POST)
	public @ResponseBody String saveFixedAsset(@RequestBody FixedAsset fixedAsset){
		try {
			
			System.out.println(fixedAsset);
			fixedAssetService.saveOrUpdate(fixedAsset);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
		return "Data Recorded!";
	}
}
