package edu.daffodil.ssb.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.daffodil.ssb.dao.CaGroups;
import edu.daffodil.ssb.services.AccCaGroupsService;

@Controller
public class AccountingGroupController {
	@Autowired
	private AccCaGroupsService accCaGroupsService;
	
	@RequestMapping("/accountinggroup")
	public String showHome(){
		return "accountinggroup";
	}
	
@RequestMapping(value="/saveAccCaGroups/{cagId}",method=RequestMethod.POST)
public @ResponseBody String saveAccCaGroups(@RequestBody CaGroups accCaGroups, @PathVariable("cagId") int cagId){		
if(cagId< 1){
	accCaGroupsService.saveAccCaGroups(accCaGroups);
}
else{
	accCaGroups.setCagId(cagId);
	accCaGroupsService.updateAccCaGroups(accCaGroups);
}
			
return "!! Record Saved Successfully";
}

@RequestMapping(value="/deleteAccCaGroups", method=RequestMethod.POST)
public @ResponseBody String deleteAccCaGroups(@RequestParam("cagId") int cagId){
	CaGroups accCaGroups = accCaGroupsService.showAccCaGroupById(cagId);
	if(accCaGroups != null){
		accCaGroupsService.deleteAccCaGroups(accCaGroups);
	}
	return "Successfully Deleted.";
}

@RequestMapping(value="/showAccCaGroups",method=RequestMethod.POST)
public @ResponseBody List<CaGroups> showAccCaGroupTable(){		
		
		return accCaGroupsService.showAccCaGroupTable();
			
	}
	
@RequestMapping(value="/showParent",method=RequestMethod.POST)
public @ResponseBody List<CaGroups> showAccCaGroup(){		
		
		return accCaGroupsService.showAccCaGroup();
			
	}

}
